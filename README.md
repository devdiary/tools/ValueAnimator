### [DeveloperDiary](https://gitlab.com/devdiary)
## [Tools](https://gitlab.com/devdiary/tools)
# ValueAnimator
Change the value in time and in range

<div align="center">
  <img src="media/icon.png"/>
</div>
<div align="center">
  <strong>source for <a href="https://medium.com/developerdiary/tools">medium</a></strong>
</div>

#### Animate integer from `-1000` to `1000` in `1000` milliseconds.

```java
new ValueAnimator.Animator(1000, -1000, 1000, new ValueAnimator.Updater()
{
  public void update(int value)
  {
  	System.out.println(value);
  }
}).animate();
```
