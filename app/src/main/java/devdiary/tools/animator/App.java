package devdiary.tools.animator;

public class App
{
    static public void main(String[] args)
    {
        new ValueAnimator.Animator(1000, -1000, 1000, new ValueAnimator.Updater()
        {
            public void update(int value)
            {
                System.out.println(value);
            }
        }).animate();
    }
}