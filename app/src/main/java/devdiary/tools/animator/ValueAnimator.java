package devdiary.tools.animator;

public interface ValueAnimator
{
    interface Updater
    {
        void update(int value);
    }
    class Animator
    {
        private final long time;
        private final int beginValue;
        private final int endValue;
        private final Updater updater;

        public Animator(long time, int beginValue, int endValue, Updater updater)
        {
            this.time = time;
            this.beginValue = beginValue;
            this.endValue = endValue;
            this.updater = updater;
        }

        public void animate()
        {
            new Thread(new Runnable()
            {
                public void run()
                {
                    long timeBegin = System.currentTimeMillis();
                    long timeEnd = timeBegin + time;
                    long timeNow = timeBegin;
                    while(timeNow < timeEnd)
                    {
                        updater.update(beginValue + (int)((timeNow-timeBegin)*(endValue - beginValue)/time));
                        timeNow = System.currentTimeMillis();
                    }
                    updater.update(endValue);
                }
            }).start();
        }
    }
}